PayUmoney for Ubercart

Synopsis
---------
This module serve as Payment Gateway porvided by PayUMoney.

Main Module: uc_payumoney.module
Requried Modules: commerce, pmphone(provided in module itself)

Instructions
------------
1. Install the module.
2. Goto Store>>Payment Methods. 
3. Select PayUMoney and click on “settings” against it. 
4. You can either choose TEST or LIVE based on whether
5. you are using Live Merchant Key or Salt or the demo Merchant Key and Salt.